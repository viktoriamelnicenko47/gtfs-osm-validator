from functools import partial

import overpy
import pyproj
import shapely.geometry as geometry
from shapely.ops import linemerge, transform, nearest_points

from gtfs_queries import get_gtfs_shape_lat_lon

geod = pyproj.Geod(ellps='WGS84')
project_to_meters = partial(
    pyproj.transform,
    pyproj.Proj('EPSG:4326'),
    pyproj.Proj('EPSG:32633'))


def _create_linestring_from_osm_response(way_id, response):
    way = None
    for response_way in response.ways:
        if way_id != response_way.id:
            continue
        way = response_way
        break
    if not way:
        return None
    ls_coords = []
    for node in way.nodes:
        ls_coords.append((node.lon, node.lat))
    ls = geometry.LineString(ls_coords)
    return ls


def is_shape_connected(relation, response):
    """
    Try to build shapely polygon out of this data. If it is merges nicely into LineString, it is fully connected
    (e.g. there are no gaps in route shape) and in that case return True. False otherwise.
    NOTE: this doesn't work if there are closed loops, as linemerge will just join them as separate LineString and
    we will end up with MultiLineString (basically, this algorithm has some false negatives)
    """
    ways = [way.ref for way in relation.members if way.role == '' and type(way) == overpy.RelationWay]
    lss = []
    original_first_point = None  # Used to reorient way, if linemerge changes orientation
    deferred_coords = []  # Used when we encounter roundabout, to defer adding it to linestring until we have next way.
    for way in ways:
        ls = _create_linestring_from_osm_response(way, response)
        if not original_first_point:
            original_first_point = geometry.Point(ls.coords[1][0], ls.coords[0][1])
        if not ls:
            return None
        if ls.is_ring:
            if len(lss) == 0:
                deferred_coords.append(ls.centroid)
            else:
                deferred_coords.append(lss[-1].coords[-1])
        else:
            if len(deferred_coords) > 0:
                deferred_coords.append(ls.coords[0])
                lss.append(geometry.LineString(deferred_coords))
                deferred_coords = []  # reset deferred
            lss.append(ls)

    merged_way = linemerge([*lss])
    return merged_way.type == 'LineString'


def create_shape_from_osm_response(relation, response):
    """
    Tries to create shapely object (LineString) from OSM route. This method works ONLY if route do not have any gaps.
    Otherwise, results can be unexpected.
    It does so by iterating for all ways, their nodes and building shape. If there are roundabouts, they are skipped
    (they will be simply bridged over center of circle, effectively joining last node of previous way and first stop
    of next way).
    """
    way_refs = [way.ref for way in relation.members if way.role == '' and type(way) == overpy.RelationWay]
    lss = []
    aligned_nodes = []
    first_way = None
    previous_way = None
    for idx, way_ref in enumerate(way_refs):
        current_way = next(w for w in response.ways if w.id == way_ref)
        current_way_closed = len(current_way.nodes) > 1 and current_way.nodes[0].id == current_way.nodes[-1].id
        if current_way_closed:
            previous_way = current_way
            continue
        if not first_way:
            previous_way = current_way
            first_way = current_way
        if len(aligned_nodes) == 0:
            previous_way = current_way
            aligned_nodes += current_way.nodes
            continue

        previous_way_closed = len(previous_way.nodes) > 1 and previous_way.nodes[0].id == previous_way.nodes[-1].id

        if aligned_nodes[-1].id == current_way.nodes[0].id:
            aligned_nodes += current_way.nodes[1:]
        elif aligned_nodes[-1].id == current_way.nodes[-1].id:
            aligned_nodes += (current_way.nodes[::-1])[1:]
        elif idx == 1 and aligned_nodes[0].id == current_way.nodes[-1].id:
            # If this is second way, we might need to re-orient
            aligned_nodes = aligned_nodes[::-1]
            aligned_nodes += (current_way.nodes[::-1])[1:]
        elif idx == 1 and aligned_nodes[0].id == current_way.nodes[0].id:
            aligned_nodes = aligned_nodes[::-1]
            aligned_nodes += current_way.nodes[1:]
        elif previous_way_closed:
            if current_way.nodes[0].id in [n.id for n in previous_way.nodes]:
                aligned_nodes += current_way.nodes
            elif current_way.nodes[-1].id in [n.id for n in previous_way.nodes]:
                aligned_nodes += current_way.nodes[::-1]
            else:
                return None
                # raise Exception('Unknown edge case, file an issue with a repro!')
        else:
            return None
            # raise Exception('Unknown edge case, file an issue with a repro!')
        previous_way = current_way

    # If our first way was oriented badly, just reverse it fully
    if aligned_nodes[0].id != first_way.nodes[0].id:
        aligned_nodes = aligned_nodes[::-1]

    coords = [(n.lon, n.lat) for n in aligned_nodes]
    merged_way = geometry.LineString(coords[:-1])
    return merged_way


def get_route_distance_in_m(ls):
    """
    Returns total distance of the route in meters
    """
    ls2 = transform(project_to_meters, ls)
    return ls2.length


def create_shape_from_gtfs(gtfs_db, shape_id):
    """
    Return shapely object (LineString) that represents GTFS shape.
    If shape is not present, returns None.
    """
    shape_df = get_gtfs_shape_lat_lon(gtfs_db, shape_id)
    if shape_df.shape[0] < 2:
        return None

    ls_coords = []
    for lon_lat in shape_df.iterrows():
        ls_coords.append((lon_lat[1]['lon'], lon_lat[1]['lat']))
    ls = geometry.LineString(ls_coords)
    return ls


def get_distance_between_routes(ls1, ls2):
    """
    Calculates Hausdorff distance between routes (in meters) and returns it
    """
    ls1 = transform(project_to_meters, ls1)
    ls2 = transform(project_to_meters, ls2)
    return ls1.hausdorff_distance(ls2)


def get_stop_distance(stop_lon, stop_lat, route):
    """
    Return distance (in meters) of stop to nearest point in route
    """
    route = transform(project_to_meters, route)
    bus_stop = transform(project_to_meters, geometry.Point(stop_lon, stop_lat))
    point_on_route, _ = nearest_points(route, bus_stop)
    return point_on_route.distance(bus_stop)


def is_stop_on_right_side(stop_lon, stop_lat, route):
    """
    Returns True if stop defined with (stop_lon, stop_lat) is on the right side of route geometry
    (which is oriented from first to last stop)
    If it cannot be determines if it is right or left, returns None
    """
    route = transform(project_to_meters, route)
    bus_stop = transform(project_to_meters, geometry.Point(stop_lon, stop_lat))

    point_on_route, _ = nearest_points(route, bus_stop)
    vector_stop_nearest = (point_on_route.coords[0][0] - bus_stop.coords[0][0], point_on_route.coords[0][1] - bus_stop.coords[0][1])
    if point_on_route.distance(bus_stop) <= 0.1:
        # If stop is too near to the shape, there might be false positives, so just ignore this stop
        return None
    distance_from_start = route.project(point_on_route)
    point_before_stop = route.interpolate(distance_from_start - 10)
    point_after_stop = route.interpolate(distance_from_start + 10)
    vector_along_route = (point_after_stop.coords[0][0] - point_before_stop.coords[0][0], point_after_stop.coords[0][1] - point_before_stop.coords[0][1])
    if vector_stop_nearest[0] * vector_stop_nearest[1] >= 0 and vector_stop_nearest[0] * vector_along_route[0] <= 0:
        return True
    if vector_stop_nearest[0] * vector_stop_nearest[1] <= 0 and vector_stop_nearest[0] * vector_along_route[0] >= 0:
        return True
    return False


def check_oneway_problems(osm_route, osm_route_data):
    """
    Notify user of any oneway ways that are in wrong direction.
    Implementation: we go from first way until last way. We are trying to connect previous way to current way, but all
    we care are non-connected nodes from previous way. When we get new way, we figure out and remember what is
    non-connected side of current way. If current way is oneway, we want to be sure that non-connected nodes are only
    connected to first node in current way. If we encounter roundabouts, we treat all nodes as non-connected as we can
    exit out of it from any node and it is all fine. When oneway is first way, we cannot determine (yet) if it is
    oriented properly, so we wait for another way and check only then.
    """
    previous_way = None
    is_previous_way_oneway = False
    non_connected_node_ids = []  # List of all non-connected nodes from previous way
    members = [way.ref for way in osm_route.members if way.role == '' and type(way) == overpy.RelationWay]
    for idx, member in enumerate(members):
        current_way = next(w for w in osm_route_data.ways if w.id == member)
        current_way_closed = len(current_way.nodes) > 1 and current_way.nodes[0].id == current_way.nodes[-1].id
        is_oneway = (not current_way_closed) and 'oneway' in current_way.tags and current_way.tags['oneway'] == 'yes'
        if not previous_way:
            # This is first way, just remember it. Both side of way are treated as non-connected.
            previous_way = current_way
            is_previous_way_oneway = is_oneway
            non_connected_node_ids = [current_way.nodes[0].id, current_way.nodes[-1].id]
            continue
        if is_oneway:
            if current_way.nodes[0].id not in non_connected_node_ids:
                print('    Route is having a oneway {0} ({1}, {2} nodes) in wrong direction'.format(
                    current_way.tags['name'] if 'name' in current_way.tags else '', current_way.id,
                    len(current_way.nodes)))
        if is_previous_way_oneway and idx == 1:
            # Check case where first way was oneway, but we still couldn't orient ourselves. Now that we have second
            # way, we can determine if first way for oriented in proper direction
            if previous_way.nodes[-1].id not in [n.id for n in current_way.nodes]:
                print('    Route is having a first oneway way {0} ({1}, {2} nodes) in wrong direction'.format(
                    previous_way.tags['name'] if 'name' in previous_way.tags else '', previous_way.id,
                    len(previous_way.nodes)))
        if current_way_closed:
            non_connected_node_ids = [n.id for n in current_way.nodes]
            continue
        if current_way.nodes[0].id in non_connected_node_ids:
            non_connected_node_ids = [current_way.nodes[-1].id, ]
        elif current_way.nodes[-1].id in non_connected_node_ids:
            non_connected_node_ids = [current_way.nodes[0].id, ]
        else:
            non_connected_node_ids = [current_way.nodes[0].id, current_way.nodes[-1].id]

        previous_way = current_way
        is_previous_way_oneway = is_oneway


def _check_oneway_problems_bogus(osm_route, osm_route_data, geom_osm_route):
    """
    Implementation that turned out as dead-end. Keeping for some time. Problem with it is when there are closed loops
    with oneways and `last_node_distance` can return lower value than `first_node_distance` (because it is already
    encountered in shape)
    """
    for member in osm_route.members:
        if member.role != "":
            continue
        if type(member) != overpy.RelationWay:
            print("    Route member ({0} without role which is not a route way found in route".format(member.ref))
            continue
        current_way = next(w for w in osm_route_data.ways if w.id == member.ref)
        is_oneway = 'oneway' in current_way.tags and current_way.tags['oneway'] == 'yes'
        if not is_oneway:
            continue
        first_node = geometry.Point(current_way.nodes[0].lon, current_way.nodes[0].lat)
        last_node = geometry.Point(current_way.nodes[-1].lon, current_way.nodes[-1].lat)
        first_node_nearest, _ = nearest_points(geom_osm_route, first_node)
        last_node_nearest, _ = nearest_points(geom_osm_route, last_node)
        first_node_distance = geom_osm_route.project(first_node_nearest)
        last_node_distance = geom_osm_route.project(last_node_nearest)
        if first_node_distance > last_node_distance:
            # This road is going in wrong direction
            print('    Route is having a oneway {0} ({1} {2}) in wrong direction'.format(
                current_way.tags['name'] if 'name' in current_way.tags else '', current_way.id, len(current_way.nodes)
            ))
