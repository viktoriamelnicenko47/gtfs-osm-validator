import pandas as pd


def get_gtfs_agency(gtfs_db, agency_id):
    agency_df = pd.read_sql_query(
        "SELECT * FROM agencies".format(agency_id=agency_id),
        gtfs_db.conn)
    if agency_id is None and agency_df.shape[0] > 1:
        raise Exception(
            "There are more than one agency in GTFS. Please specify agency for which to do check by setting "
            "AGENCY_ID in config.py")
    elif agency_id is not None:
        selected_agency_df = agency_df.query("agency_id=='{0}'".format(agency_id))
        if selected_agency_df.shape[0] == 0:
            raise Exception("Agency with id {0} do not exist in GTFS data".format(agency_id))
        return selected_agency_df.iloc[0]
    return agency_df.iloc[0]


def get_gtfs_services(gtfs_db):
    a_df = pd.read_sql_query("SELECT * FROM calendar", gtfs_db.conn)
    return a_df


def get_all_gtfs_routes(gtfs_db):
    p_df = pd.read_sql_query("SELECT * FROM routes", gtfs_db.conn)
    return p_df


def get_all_gtfs_trips(gtfs_db, route_I):
    trips_df = pd.read_sql_query("SELECT * FROM trips WHERE route_I={route_I}".format(route_I=route_I), gtfs_db.conn)
    return trips_df


def get_all_gtfs_stops_in_trip(gtfs_db, trip_I):
    stops_df = pd.read_sql_query("""
        SELECT * FROM stop_times st, stops s ON st.stop_I=s.stop_I
        WHERE st.trip_I={trip_I}
        ORDER BY seq
    """.format(trip_I=trip_I), gtfs_db.conn)
    return stops_df


def get_gtfs_interval(gtfs_db, route_I, service_I, shape_id, direction_id):
    """
    Tries to get median value of all departure times for a given service (optional) and either shape or direction,
    so we can estimate interval.
    :return: Interval, in seconds
    """
    if not shape_id and not direction_id:
        return None
    shape_condition = "AND t.shape_id = '{shape_id}'".format(shape_id=shape_id) if shape_id else ""
    direction_condition = "AND t.direction_id = '{direction_id}'".format(direction_id=direction_id) if direction_id else ""
    service_condition = "AND t.service_I = {service_I}".format(service_I=service_I) if service_I else ""
    dep_times_df = pd.read_sql_query("""
        SELECT * FROM trips t, stop_times st ON t.trip_I=st.trip_I
        WHERE t.route_I = {route_I} AND st.seq=1 {service_condition} {shape_condition} {direction_condition}
        ORDER BY dep_time_ds
    """.format(route_I=route_I, service_condition=service_condition, shape_condition=shape_condition,
               direction_condition=direction_condition), gtfs_db.conn)

    if dep_times_df.shape[0] == 0:
        # There are no rows, no point getting interval
        return None

    total_interval_sec = int(
        dep_times_df[['service_I', 'dep_time_ds']].groupby('service_I').diff()['dep_time_ds'].dropna().median()
    )

    return total_interval_sec


def get_gtfs_shape_duration(gtfs_db, route_I, service_I, shape_id, direction_id):
    """
    Tries to get median value od total travel time (duration) for all trips
    for a given service (optional) and either shape or direction.
    :return: Duration, in seconds
    """
    shape_condition = "AND t.shape_id = '{shape_id}'".format(shape_id=shape_id) if shape_id else ""
    direction_condition = "AND t.direction_id = '{direction_id}'".format(direction_id=direction_id) if direction_id else ""
    service_condition = "AND service_I = {service_I}".format(service_I=service_I) if service_I else ""

    durations_df = pd.read_sql_query("""
        WITH departure AS (
            SELECT t.trip_I, start_time_ds FROM trips t, stop_times st ON t.trip_I=st.trip_I
            WHERE t.route_I = {route_I} {service_condition} AND st.seq=1 {shape_condition} {direction_condition}
        ),
        arrivals AS (
            SELECT t.trip_I, arr_time_ds FROM trips t, stop_times st ON t.trip_I=st.trip_I
            WHERE t.route_I = {route_I} {service_condition} AND st.seq=(
                SELECT MAX(seq) FROM trips t, stop_times st ON t.trip_I=st.trip_I
                WHERE t.route_I = {route_I} {service_condition} {shape_condition} {direction_condition}
            ) {shape_condition} {direction_condition}
        )
        SELECT arr_time_ds-start_time_ds
        FROM departure d JOIN arrivals a ON d.trip_I=a.trip_I
    """.format(route_I=route_I, service_condition=service_condition, shape_condition=shape_condition,
               direction_condition=direction_condition), gtfs_db.conn)
    if durations_df.shape[0] == 0:
        # There are no rows, no point getting duration
        return None

    total_duration_sec = int(durations_df.median())
    return total_duration_sec


def get_gtfs_first_stop(gtfs_db, route_I):
    """
    Try to find most common stop for first stop and return map of direction->most common first stop
    """
    stops_df = pd.read_sql_query("""
        SELECT direction_id, name
        FROM stop_times st
            JOIN stops s ON st.stop_I=s.stop_I
            JOIN trips t ON t.trip_I=st.trip_I
        WHERE t.route_I={route_I} AND st.seq=1
        ORDER BY direction_id, seq
    """.format(route_I=route_I), gtfs_db.conn)
    return stops_df.groupby('direction_id').agg(lambda x: x.value_counts().index[0])


def get_gtfs_first_stop_with_shape(gtfs_db, route_I, shape_id):
    """
    Try to find most common stop for first stop when we know shape_id
    """
    stops_df = pd.read_sql_query("""
        SELECT name
        FROM stop_times st
            JOIN stops s ON st.stop_I=s.stop_I
            JOIN trips t ON t.trip_I=st.trip_I
        WHERE t.route_I={route_I} AND t.shape_id='{shape_id}' AND st.seq=1
    """.format(route_I=route_I, shape_id=shape_id), gtfs_db.conn)
    most_common_stop = stops_df.agg(lambda x: x.value_counts().index[0])
    if most_common_stop.shape[0] == 0:
        return None
    return most_common_stop.iloc[0]


def get_gtfs_last_stop(gtfs_db, route_I):
    """
    Try to find most common last stop and return map of direction->most common last stop
    """
    stops_df = pd.read_sql_query("""
        WITH max_stops AS (
            SELECT t.trip_I, MAX(st.seq) max_stop_seq
            FROM stop_times st JOIN trips t ON t.trip_I=st.trip_I
            WHERE t.route_I={route_I}
            GROUP BY t.trip_I
        ), trip_stops AS (
            SELECT t.trip_I, t.direction_id, s.name, st.seq
            FROM stop_times st
            JOIN stops s ON st.stop_I=s.stop_I
            JOIN trips t ON t.trip_I=st.trip_I
            WHERE t.route_I={route_I}
        )
        SELECT ts.direction_id, ts.name
        FROM trip_stops ts JOIN max_stops ms ON ts.trip_I=ms.trip_I AND ts.seq=ms.max_stop_seq
        ORDER BY ts.direction_id, ts.seq
    """.format(route_I=route_I), gtfs_db.conn)
    return stops_df.groupby('direction_id').agg(lambda x: x.value_counts().index[0])


def get_gtfs_last_stop_with_shape(gtfs_db, route_I, shape_id):
    """
    Try to find most common last stop when we know shape
    """
    stops_df = pd.read_sql_query("""
        WITH max_stops AS (
            SELECT t.trip_I, MAX(st.seq) max_stop_seq
            FROM stop_times st JOIN trips t ON t.trip_I=st.trip_I
            WHERE t.route_I={route_I} AND shape_id='{shape_id}'
            GROUP BY t.trip_I
        ), trip_stops AS (
            SELECT t.trip_I, s.name, st.seq
            FROM stop_times st
            JOIN stops s ON st.stop_I=s.stop_I
            JOIN trips t ON t.trip_I=st.trip_I
            WHERE t.route_I={route_I} AND shape_id='{shape_id}'
        )
        SELECT ts.name
        FROM trip_stops ts JOIN max_stops ms ON ts.trip_I=ms.trip_I AND ts.seq=ms.max_stop_seq
    """.format(route_I=route_I, shape_id=shape_id), gtfs_db.conn)
    most_common_stop = stops_df.agg(lambda x: x.value_counts().index[0])
    if most_common_stop.shape[0] == 0:
        return None
    return most_common_stop.iloc[0]


def get_gtfs_trip_by_first_and_last_stop(gtfs_db, route_I, service_I, shape_id, direction_id, first_stop, last_stop):
    if not first_stop or not last_stop:
        return pd.DataFrame(columns=['trip_id'])

    shape_condition = "AND t.shape_id = '{shape_id}'".format(shape_id=shape_id) if shape_id else ""
    direction_condition = "AND t.direction_id = '{direction_id}'".format(direction_id=direction_id) if direction_id else ""
    gtfs_proper_trips_df = pd.read_sql_query("""
        WITH max_stops AS (
            SELECT t.trip_I, t.trip_id, MAX(st.seq) max_stop_seq
            FROM stop_times st JOIN trips t ON t.trip_I=st.trip_I
            WHERE t.route_I={route_I} AND t.service_I={service_I} {shape_condition} {direction_condition}
            GROUP BY t.trip_I, t.trip_id
        ), trip_stops AS (
            SELECT t.trip_I, t.trip_id, t.direction_id, s.name, st.seq
            FROM stop_times st
                JOIN stops s ON st.stop_I=s.stop_I
                JOIN trips t ON t.trip_I=st.trip_I
            WHERE t.route_I={route_I} AND t.service_I={service_I} {shape_condition} {direction_condition}
        ), trips_with_proper_last_stop AS (
            SELECT ts.trip_I, ts.trip_id
            FROM trip_stops ts
                JOIN max_stops ms ON ts.trip_I=ms.trip_I AND ts.seq=ms.max_stop_seq
            WHERE name = '{last_stop}'
        ), trips_with_proper_first_stop AS (
            SELECT t.trip_I, t.trip_id
            FROM stop_times st
                JOIN stops s ON st.stop_I=s.stop_I
                JOIN trips t ON t.trip_I=st.trip_I
            WHERE t.route_I={route_I} AND t.service_I={service_I} AND st.seq=1 AND s.name = '{first_stop}' {shape_condition} {direction_condition}
        )
        SELECT tfs.trip_I, tfs.trip_id
        FROM trips_with_proper_last_stop tls
            JOIN trips_with_proper_first_stop tfs ON tls.trip_I=tfs.trip_I
    """.format(
        route_I=route_I, service_I=service_I, shape_condition=shape_condition, direction_condition=direction_condition,
        first_stop=first_stop, last_stop=last_stop),
        gtfs_db.conn)
    return gtfs_proper_trips_df


def get_gtfs_shape_lat_lon(gtfs_db, shape_id):
    shape_df = pd.read_sql_query("""
        SELECT lat, lon FROM shapes where shape_id='{shape_id}' ORDER BY seq
        """.format(shape_id=shape_id), gtfs_db.conn)
    return shape_df


def get_all_gtfs_route_variants(gtfs_db, route_I):
    route_variants_df = pd.read_sql_query("""
        with stop_list as (
            select s.stop_id, s.name, st.seq, t.trip_id, t.trip_I
            from stops s
                inner join stop_times st on s.stop_I=st.stop_I
                inner join trips t on st.trip_I=t.trip_I
                inner join routes r on t.route_I=r.route_I
            where r.route_I='{route_I}'
            order by t.trip_id, st.seq
        ),
        grouped_stops AS (
            select trip_id, trip_I, group_concat(stop_id || ':' || name || ':' || seq) stops
            from stop_list
            group by trip_id, trip_I
        )
        select stops, min(trip_I) trip_I_sample, min(trip_id) trip_id_sample, group_concat(trip_id) trips, count(*) trip_count from grouped_stops
        group by stops
        order by trip_count desc
    """.format(route_I=route_I), gtfs_db.conn)
    return route_variants_df
